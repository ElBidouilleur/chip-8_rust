pub const CELL_SIZE: usize = 10;
pub const GRID_WIDTH: usize = 64;
pub const GRID_HEIGHT: usize = 32;

pub const WIDTH: usize = CELL_SIZE * GRID_WIDTH;
pub const HEIGHT: usize = CELL_SIZE * GRID_HEIGHT;

pub const CELL_WIDTH: usize = GRID_WIDTH;
pub const CELL_HEIGHT: usize = GRID_HEIGHT;

pub struct Chip8State {
    address_register: usize,
    grid: Vec<bool>,
    registers: [u8; 16],
    stack: Vec<u16>,
    sound_timer: u16,
    delay_time: u16,
    source: Vec<u8>,
}

impl Chip8State {
    pub fn new(source: Vec<u8>) -> Chip8State {
        Chip8State {
            address_register: 0,
            delay_time: 0,
            grid: vec![false; GRID_WIDTH * GRID_WIDTH],
            registers: [0; 16],
            sound_timer: 0,
            source,
            stack: Vec::new(),
        }
    }

    fn decode_next_instruction(&self) {
        let opcode = (self.source[self.address_register] as u16) << 8 & self.source[self.address_register + 1] as u16;

        println!("Opcode: {:x}", opcode);
    }

    pub fn tick(&mut self) {
        self.decode_next_instruction();
    }

    pub fn set_source(&mut self, source: Vec<u8>) {
        self.source = source;
    }
}