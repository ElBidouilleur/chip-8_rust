mod chip8;

extern crate minifb;

use std::{env, fs};
use crate::chip8::state::{GRID_WIDTH, WIDTH, HEIGHT, CELL_WIDTH, CELL_HEIGHT, CELL_SIZE, Chip8State};

use std::time::SystemTime;

use minifb::{Key, MouseButton, MouseMode, Window, WindowOptions};

fn main() {
    let args: Vec<String> = env::args().collect();


    if args.len() <= 1 {
        println!("Usage: chip8 <rom_path>");
        return;
    }

    let rom_path = &args[1];

    let content = match fs::read(rom_path) {
        Ok(c) => c,
        Err(e) => {
            println!("Failed to open your rom, please try an other. Error {}", e);
            return;
        }
    };

    let mut state = Chip8State::new(content);

    state.tick();

    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];

    let mut grid = vec![false; CELL_WIDTH * CELL_HEIGHT];
    grid[42] = true;

    let mut window = Window::new(
        "CHIP-8 -- ESC to exit",
        WIDTH,
        HEIGHT,
        WindowOptions::default(),
    )
        .unwrap_or_else(|e| {
            panic!("{}", e);
        });

    let mut clock = SystemTime::now();

    while window.is_open() && !window.is_key_down(Key::Escape) {
        for (index, cell) in buffer.iter_mut().enumerate() {
            let x = index % WIDTH;
            let y = index / WIDTH;

            let cell_x = x / CELL_SIZE;
            let cell_y = y / CELL_SIZE;

            *cell = if grid[cell_y * CELL_WIDTH + cell_x] {
                0xFFFFFFF
            } else {
                0
            };
        }

        window.update_with_buffer(&buffer, WIDTH, HEIGHT).unwrap();
    }
}
